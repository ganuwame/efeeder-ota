#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <HTTPClient.h>
#include <HTTPUpdate.h>
#include <PubSubClient.h>
#include "config.h"

unsigned long previousMillis = 0;
const long interval = 5000;

WiFiClient espClient;
PubSubClient mqttClient(espClient);
long lastMsg = 0;

void connectWifi();
int firmwareVersionCheck();

void connectWifi()
{
  Serial.println("Waiting for WiFi");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

int firmwareVersionCheck(void)
{
  String payload;
  int httpCode = 0;
  String fwurl = "";
  fwurl += URL_fw_Version;
  fwurl += "?";
  fwurl += String(rand());

  HTTPClient https;

  if (https.begin(fwurl))
  {
    delay(100);
    httpCode = https.GET();
    delay(100);
    if (httpCode == HTTP_CODE_OK)
    {
      payload = https.getString();
    }
    else
    {
      Serial.print("error in checking version file:");
      Serial.println(httpCode);
    }
    https.end();
  }

  // if version received
  if (httpCode == HTTP_CODE_OK)
  {
    payload.trim();
    if (payload.equals(firmwareVersion))
    {
      Serial.printf("\nDevice already on latest firmware version:%s\n", firmwareVersion);
      return 0;
    }
    else
    {
      Serial.print("New firmware detected: ");
      Serial.println(payload);
      return 1;
    }
  }
  return 0;
}

void firmwareUpdate(void)
{
  WiFiClientSecure clientOTA;
  clientOTA.setCACert(rootCACertificate);
  httpUpdate.setLedPin(LED_BUILTIN, LOW);
  mqttClient.publish("eFeeder/eFishery/yogawakamenta/status", "UPDATING FIRMWARE [OTA]");
  Serial.println();
  Serial.println("[Updating Firmware]");
  Serial.println();
  t_httpUpdate_return ret = httpUpdate.update(clientOTA, URL_fw_Bin);

  switch (ret)
  {
  case HTTP_UPDATE_FAILED:
    mqttClient.publish("eFeeder/eFishery/yogawakamenta/status", "UPDATE FIRMWARE [FAILED]");
    Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
    break;

  case HTTP_UPDATE_NO_UPDATES:
    Serial.println("HTTP_UPDATE_NO_UPDATES");
    break;

  case HTTP_UPDATE_OK:
    Serial.println("HTTP_UPDATE_OK");
    break;
  }
}

void mqttCallback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if ((char)payload[0] == '1')
  {
    digitalWrite(BUILTIN_LED, LOW);
    if (firmwareVersionCheck())
    {
      firmwareUpdate();
    }
  }
  else
  {
    digitalWrite(BUILTIN_LED, HIGH);
  }
}

void reconnectMQTT()
{
  // reconnectMQTT
  while (!mqttClient.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("eFeederYogaWakamenta"))
    {
      Serial.println("connected");
      // connected, set status ACTIVE
      mqttClient.publish("eFeeder/eFishery/yogawakamenta/status", "ACTIVE");
      // subscribe
      mqttClient.subscribe("eFeeder/eFishery/yogawakamenta/update");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" reconnect in 5 seconds");
      delay(5000);
    }
  }
}

void setup()
{
  Serial.begin(115200);
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.print("Current firmware version:");
  Serial.println(firmwareVersion);
  connectWifi();
  mqttClient.setServer(mqtt_server, 1883);
  mqttClient.setCallback(mqttCallback);
}

void loop()
{
  if (!mqttClient.connected())
  {
    reconnectMQTT();
  }
  mqttClient.loop();

  unsigned long currentMillis = millis();
  if ((currentMillis - previousMillis) >= interval)
  {
    previousMillis = currentMillis;
    // Broadcast Firmware Version
    mqttClient.publish("eFeeder/eFishery/yogawakamenta/status", firmwareVersion);
    Serial.print("Current firmware version [");
    Serial.print(firmwareVersion);
    Serial.println("]");
  }
}